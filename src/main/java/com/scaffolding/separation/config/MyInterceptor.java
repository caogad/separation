package com.scaffolding.separation.config;

import com.alibaba.fastjson.JSONObject;
import com.scaffolding.separation.annotation.CheckToken;
import com.scaffolding.separation.annotation.PassToken;
import com.scaffolding.separation.utils.JwtTokenUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

public class MyInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    // 在执行目标方法之前执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        //System.out.println("preHandle");

        //***********************************************RestFul  API   JWT  Token  使用以下代码************************
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        String token = request.getHeader("token");
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        if (method.isAnnotationPresent(PassToken.class)) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if (passToken.required()) {
                return true;
            }
        }
        JSONObject res = new JSONObject();
        if (method.isAnnotationPresent(CheckToken.class)) {
            CheckToken userLoginToken = method.getAnnotation(CheckToken.class);
            if (userLoginToken.required()) {
                // 执行认证
                if (StringUtils.isEmpty(token)) {
                    return errorMsg(response, res, "无token，请重新登录");
                }

                try {
                    jwtTokenUtils.getUserId(token);
                } catch (Exception j) {
                    return errorMsg(response, res, j.getMessage());
                }

                // 验证 token
                try {
                    jwtTokenUtils.verifyToken(token);
                } catch (Exception e) {
                    return errorMsg(response, res, e.getMessage());
                }
            }
        }
        //***********************************************RestFul  API   JWT  Token  使用以上代码************************


        //进行逻辑判断，如果ok就返回true，不行就返回false，返回false就不会处理改请求
        return true;
    }

    private boolean errorMsg(HttpServletResponse response, JSONObject res, String message) throws IOException {
        PrintWriter out;
        res.put("message", message);
        res.put("code", "-1");
        out = response.getWriter();
        out.append(res.toString());
        return false;
    }

    // 执行目标方法之后执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        //System.out.println("postHandler");

    }

    // 在请求已经返回之后执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        //System.out.println("afterCompletion");

    }
}
