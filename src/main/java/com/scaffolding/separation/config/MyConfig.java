package com.scaffolding.separation.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration//关联配置文件
@ConfigurationProperties(prefix = "my")//值的前缀
@PropertySource(value = "classpath:my-spring.properties")//给属性注入值
// 不需要切换环境的,读取固定值
public class MyConfig {
    // 允许上传的文件类列表
    private String fileTypes;

}

