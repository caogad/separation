package com.scaffolding.separation.config.shiro;

import com.scaffolding.separation.sys.model.Permission;
import com.scaffolding.separation.sys.model.Role;
import com.scaffolding.separation.sys.model.User;
import com.scaffolding.separation.sys.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @program: shiro
 * @description: 自定义Realm
 * @author: 01
 * @create:
 **/
public class ShiroRealm extends AuthorizingRealm {

    private Logger logger = LogManager.getLogger(ShiroRealm.class);

    @Autowired
    private UserService userService;

    // 认证登录
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 将AuthenticationToken强转为AuthenticationToken对象
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        logger.info("验证当前Subject时获取到token为：" + token.toString());
        //查出是否有此用户
        String username = token.getUsername();
        //通过username从数据库中查找 User对象，如果找到，没找到.
        //这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        User userInfo = userService.findByUserName(username);
        if (userInfo == null) {
            throw new UnknownAccountException();
        }
        if ("0".equals(userInfo.getStatus().toString())) {
            throw new LockedAccountException(); //帐号锁定
        }

        /*
         * 获取权限信息:这里没有进行实现，
         * 请自行根据UserInfo,Role,Permission进行实现；
         * 获取之后可以在前端for循环显示所有链接
         */
        //userInfo.setPermissions(userService.findPermissions(member))

        //账号判断

        //加密方式
        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以自定义实现
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                userInfo, //用户名
                userInfo.getPswd(), //密码
                ByteSource.Util.bytes(userInfo.getCredentialsSalt()),//salt=username+salt
                userInfo.getNickname()  //realm name
        );
        return authenticationInfo;
    }

    // 授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        logger.info("##################执行Shiro权限认证##################");
        User user = (User) principalCollection.getPrimaryPrincipal();
        if (user != null) {
            List<String> permissionList = new ArrayList<>();
            Set<String> roleNameSet = new HashSet<>();

            // 获取用户的角色集
            Set<Role> roleSet = user.getRoles();
            if (!CollectionUtils.isEmpty(roleSet)) {
                for (Role role : roleSet) {
                    // 添加角色名称
                    roleNameSet.add(role.getName());
                    // 获取角色的权限集
                    Set<Permission> permissionSet = role.getPermissions();
                    if (!CollectionUtils.isEmpty(permissionSet)) {
                        for (Permission permission : permissionSet) {
                            // 添加权限名称
                            permissionList.add(permission.getName());
                        }
                    }
                }
            }
            //权限信息对象authorizationInfo,用来存放查出的用户的所有的角色（role）及权限（permission）
            SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
            //用户的角色集合
            authorizationInfo.addRoles(roleNameSet);
            //用户的权限集合
            authorizationInfo.addStringPermissions(permissionList);
            return authorizationInfo;
        }
        // 返回null的话，就会导致任何用户访问被拦截的请求时，都会自动跳转到unauthorizedUrl指定的地址
        return null;
    }


}
