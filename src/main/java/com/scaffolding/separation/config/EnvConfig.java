package com.scaffolding.separation.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "envconfig")
// 需要切换环境的
public class EnvConfig {
    // 当前环境URL地址
    private String envurl;
    // 百度云推送
    private String baiDuPushAppId;
    private String baiDuPushApiKey;
    private String baiDuPushSecretKey;
    private String path;
    private String url;
    private String AWS_ACCESS_KEY; // 【你的 access_key】
    private String AWS_SECRET_KEY; // 【你的 aws_secret_key】
    private String bucketName; // 【你 bucket 的名字】 # 首先需要保证 s3 上已经存在该存储桶

}
