package com.scaffolding.separation.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffolding.separation.sys.model.UserRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户ID查找角色
     *
     * @param userid
     * @return UserRole
     */
    @Select("SELECT * FROM user_role ur LEFT JOIN role r ON ur.rid = r.id WHERE uid = #{userid}")
    List<UserRole> findUserRoleByUserID(@Param("userid") String userid);
}
