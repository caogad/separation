package com.scaffolding.separation.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffolding.separation.sys.model.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface RoleMapper extends BaseMapper<Role> {

}
