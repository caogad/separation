package com.scaffolding.separation.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scaffolding.separation.sys.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名查找用户
     *
     * @param username 用户名
     * @return user
     */
    @Select("SELECT * FROM user where nickname = #{username}")
    User findByUserName(@Param("username") String username);
}
