package com.scaffolding.separation.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@RestController
@RequestMapping("/sys/permission")
public class PermissionController {

}

