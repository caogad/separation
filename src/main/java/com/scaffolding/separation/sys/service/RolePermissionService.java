package com.scaffolding.separation.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.separation.sys.model.RolePermission;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface RolePermissionService extends IService<RolePermission> {

}
