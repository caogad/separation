package com.scaffolding.separation.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.separation.sys.model.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserService extends IService<User> {

    /**
     * 根据用户名查找用户
     *
     * @param username 用户名
     * @return user
     */
    User findByUserName(String username);
}
