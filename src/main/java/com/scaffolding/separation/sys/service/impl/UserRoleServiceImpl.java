package com.scaffolding.separation.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.scaffolding.separation.sys.mapper.UserRoleMapper;
import com.scaffolding.separation.sys.model.UserRole;
import com.scaffolding.separation.sys.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

   @Override
   public List<UserRole> findUserRoleByUserID(String userid) {
       return userRoleMapper.findUserRoleByUserID(userid);
   }
}
