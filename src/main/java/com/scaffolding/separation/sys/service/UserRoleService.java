package com.scaffolding.separation.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.separation.sys.model.UserRole;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 根据用户ID查找角色
     *
     * @param userid
     * @return UserRole
     */
    List<UserRole> findUserRoleByUserID(String userid);
}
