package com.scaffolding.separation.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.scaffolding.separation.sys.model.Role;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author admin
 * @since 2019-06-21
 */
public interface RoleService extends IService<Role> {

}
