package com.scaffolding.separation.utils;

import com.amazonaws.util.IOUtils;
import com.scaffolding.separation.config.EnvConfig;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.nio.file.Files;

/**
 * fMultipartFile 与 File 互转
 */
@Configuration
@EnableConfigurationProperties(EnvConfig.class)
public class FileUtils {
    /**
     * MultipartFile 转 File
     *
     * @param file
     * @throws Exception
     */
    public File multipartFileToFile(@RequestParam MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }

    /**
     * File 转 MultipartFile
     *
     * @param file
     * @throws Exception
     */
    public void fileToMultipartFile(File file) throws Exception {

        //MockMultipartFile  spring-test找不到
        /*FileInputStream fileInput = new FileInputStream(file);
        MultipartFile toMultipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(fileInput));
        toMultipartFile.getInputStream();*/

        FileItem fileItem = new DiskFileItem("mainFile", Files.probeContentType(file.toPath()), false, file.getName(), (int) file.length(), file.getParentFile());
        try (
                InputStream input = new FileInputStream(file);
                OutputStream os = fileItem.getOutputStream();) {
                IOUtils.copy(input, os);
                // Or faster..
                //IOUtils.copy(new FileInputStream(file), fileItem.getOutputStream());
        } catch (IOException ex){
            // do something.
        }
        MultipartFile mulFile = new CommonsMultipartFile(fileItem);
    }


    public void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除临时文件
     */
//        File del = new File(file.toURI());
//        del.delete();

}
