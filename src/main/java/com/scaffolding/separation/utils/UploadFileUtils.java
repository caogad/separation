package com.scaffolding.separation.utils;

import com.scaffolding.separation.config.MyConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Configuration
@EnableConfigurationProperties(MyConfig.class)
public class UploadFileUtils {

    @Autowired
    private MyConfig myConfig;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 单个文件上传
     *
     * @param file
     * @param path 存放路径
     */
//    public void fileUpLoad(MultipartFile file, String path) throws Exception {
//        String fileName = null;
//        if (!file.isEmpty()) {
//            try {
//                fileName = file.getOriginalFilename();
//                checkFileType(fileName);
//                logger.info("文件名字是:" + fileName);
//                file.transferTo(buildFile(path + File.separator + fileName, false));
//            } catch (Exception e) {
//                logger.error(e.getMessage());
//                throw new Exception(e.getMessage());
//            }
//        } else {
//            logger.error("上传的文件是空的.");
//            throw new Exception("上传的文件是空的.");
//        }
//    }

    public String fileUpLoad(MultipartFile file, String path) throws Exception {
        String fileName = null;
        if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();
                checkFileType(fileName);
                logger.info("文件名字是：" + fileName);
                file.transferTo(buildFile(path + File.separator + fileName, false));
            } catch (Exception e) {
                logger.error(e.getMessage());
                throw new Exception(e.getMessage());
            }
        } else {
            logger.error("上传的文件是空的。");
            throw new Exception("上传的文件是空的。");
        }
        return path + File.separator + fileName;
    }

    /**
     * 多文件上传
     * @param files
     * @param path
     * @throws Exception
     */
    public void fileUpLoad(MultipartFile[] files, String path) throws Exception {
        String fileName = null;
        if (files != null && files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                try {
                    fileName = files[i].getOriginalFilename();
                    checkFileType(fileName);
                    byte[] bytes = files[i].getBytes();
                    BufferedOutputStream buffStream =
                            new BufferedOutputStream(new FileOutputStream(buildFile(path, true)));
                    buffStream.write(bytes);
                    buffStream.close();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    throw new Exception(e.getMessage());
                }
            }
        } else {
            logger.error("上传的文件是空的.");
            throw new Exception("上传的文件是空的.");
        }
    }

    public static File buildFile(String fileName, boolean isDirectory) {
        File target = new File(fileName);
        if (isDirectory) {
            target.mkdirs();
        } else {
            if (!target.getParentFile().exists()) {
                target.getParentFile().mkdirs();
                target = new File(target.getAbsolutePath());
            }
        }
        return target;
    }

    public void checkFileType(String fileName) throws Exception {
        String fileList = myConfig.getFileTypes();
        if (StringUtils.isEmpty(fileName)) {
            throw new Exception("没有获取到文件名.");
        }
        if (fileName.indexOf(".") <= 0) {
            throw new Exception("文件扩展名不正确.");
        }
        if (!fileList.toLowerCase().contains(fileName.toLowerCase().substring(fileName.indexOf("."), fileName.length()))) {
            throw new Exception("文件类型不匹配.支持文件类型:" + myConfig.getFileTypes());
        }
    }

}
