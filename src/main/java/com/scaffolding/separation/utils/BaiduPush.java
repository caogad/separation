package com.scaffolding.separation.utils;

import com.baidu.yun.core.log.YunLogEvent;
import com.baidu.yun.core.log.YunLogHandler;
import com.baidu.yun.push.auth.PushKeyPair;
import com.baidu.yun.push.client.BaiduPushClient;
import com.baidu.yun.push.constants.BaiduPushConstants;
import com.baidu.yun.push.exception.PushClientException;
import com.baidu.yun.push.exception.PushServerException;
import com.baidu.yun.push.model.PushMsgToAllRequest;
import com.baidu.yun.push.model.PushMsgToAllResponse;
import com.baidu.yun.push.model.PushMsgToSingleDeviceRequest;
import com.baidu.yun.push.model.PushMsgToSingleDeviceResponse;
import com.scaffolding.separation.config.EnvConfig;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(EnvConfig.class)
public class BaiduPush {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private EnvConfig envConfig;

    /**
     * 向单个设备推送消息。
     *
     * @param channelId
     * @param jsonCustormCont
     * @return
     */
    public String pushMsgToSingleDevice(String channelId, JSONObject jsonCustormCont) {
        try {
            if (StringUtils.isEmpty(channelId)) {
                logger.error("channelId is null");
                return null;
            }
            if (jsonCustormCont == null) {
                logger.error("JSONObject is null");
                return null;
            }
            // 1. get apiKey and secretKey from developer console
            String apiKey = envConfig.getBaiDuPushApiKey();
            String secretKey = envConfig.getBaiDuPushSecretKey();
            PushKeyPair pair = new PushKeyPair(apiKey, secretKey);

            // 2. build a BaidupushClient object to access released interfaces
            BaiduPushClient pushClient = new BaiduPushClient(pair,
                    BaiduPushConstants.CHANNEL_REST_URL);

            // 3. register a YunLogHandler to get detail interacting information
            // in this request.
            pushClient.setChannelLogHandler(new YunLogHandler() {
                @Override
                public void onHandle(YunLogEvent event) {
                    System.out.println(event.getMessage());
                }
            });

            try {
                // 4. specify request arguments
                //创建 Android的通知
                JSONObject notification = new JSONObject();
//                notification.put("title", "TEST");
                notification.put("description", jsonCustormCont.get("message"));
//                notification.put("notification_builder_id", 0);
//                notification.put("notification_basic_style", 7);
//                notification.put("open_type", 2);
//                notification.put("url", "http://push.baidu.com");
//                notification.put("pkg_content", "");
//                JSONObject jsonCustormCont = new JSONObject();
//                jsonCustormCont.put("key", "测试"); //自定义内容，key-value
                notification.put("custom_content", jsonCustormCont);
                System.out.println(jsonCustormCont.toString());
                PushMsgToSingleDeviceRequest request = new PushMsgToSingleDeviceRequest()
                        .addChannelId(channelId)
                        .addMsgExpires(new Integer(3600)). // message有效时间
                        addMessageType(0).// 1：通知,0:透传消息. 默认为0 注：IOS只有通知.
                        addMessage(notification.toString()).
                                addDeviceType(3);// deviceType => 3:android, 4:ios
                // 5. http request
                PushMsgToSingleDeviceResponse response = pushClient
                        .pushMsgToSingleDevice(request);
                // Http请求结果解析打印
                System.out.println("msgId: " + response.getMsgId() + ",sendTime: " + response.getSendTime());
                return response.getMsgId();
            } catch (PushClientException e) {
                /*
                 * ERROROPTTYPE 用于设置异常的处理方式 -- 抛出异常和捕获异常,'true' 表示抛出, 'false' 表示捕获。
                 */
                if (BaiduPushConstants.ERROROPTTYPE) {
                    throw e;
                } else {
                    e.printStackTrace();
                }
            } catch (PushServerException e) {
                if (BaiduPushConstants.ERROROPTTYPE) {
                    logger.error(e.getMessage());
                    throw e;
                } else {
                    logger.error(String.format(
                            "requestId: %d, errorCode: %d, errorMessage: %s",
                            e.getRequestId(), e.getErrorCode(), e.getErrorMsg()));
                }
            }
        } catch (PushClientException e) {
            logger.error(e.getMessage());

        } catch (PushServerException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * 推送消息给所有设备，即广播推送。
     *
     * @param jsonCustormCont
     * @return
     */
    public String pushMsgToAll(JSONObject jsonCustormCont) {
        if (jsonCustormCont == null) {
            logger.error("JSONObject is null");
            return null;
        }
        // 1. get apiKey and secretKey from developer console
        String apiKey = envConfig.getBaiDuPushApiKey();
        String secretKey = envConfig.getBaiDuPushSecretKey();
        PushKeyPair pair = new PushKeyPair(apiKey, secretKey);

        // 2. build a BaidupushClient object to access released interfaces
        BaiduPushClient pushClient = new BaiduPushClient(pair,
                BaiduPushConstants.CHANNEL_REST_URL);

        // 3. register a YunLogHandler to get detail interacting information
        // in this request.
        pushClient.setChannelLogHandler(new YunLogHandler() {
            @Override
            public void onHandle(YunLogEvent event) {
                System.out.println(event.getMessage());
            }
        });

        try {
            JSONObject notification = new JSONObject();
            notification.put("description", jsonCustormCont.get("message"));
            notification.put("custom_content", jsonCustormCont);

            // 4. specify request arguments
            PushMsgToAllRequest request = new PushMsgToAllRequest()
                    .addMsgExpires(new Integer(3600))
                    .addMessageType(0) // 1：通知,0:透传消息. 默认为0 注：IOS只有通知.
                    .addMessage(notification.toString()) //添加透传消息
                    .addSendTime(System.currentTimeMillis() / 1000 + 65) // 设置定时推送时间，必需超过当前时间一分钟，单位秒.实例1分钟5秒后推送
                    .addDeviceType(3);
            // 5. http request
            PushMsgToAllResponse response = pushClient.pushMsgToAll(request);
            // Http请求结果解析打印
            System.out.println("msgId: " + response.getMsgId() + ",sendTime: " + response.getSendTime() + ",timerId: " + response.getTimerId());
            return response.getMsgId();
        } catch (PushClientException e) {
            if (BaiduPushConstants.ERROROPTTYPE) {
                logger.error(e.getMessage());
            } else {
                e.printStackTrace();
            }
        } catch (PushServerException e) {
            if (BaiduPushConstants.ERROROPTTYPE) {
                logger.error(e.getMessage());
            } else {
                System.out.println(String.format("requestId: %d, errorCode: %d, errorMessage: %s", e.getRequestId(), e.getErrorCode(), e.getErrorMsg()));
            }
        }
        return null;
    }

    public static void main(String[] args)
            throws PushClientException, PushServerException {
//        // 1. get apiKey and secretKey from developer console
//        String apiKey = "TvftqczKidDjepI3d9CxQfzO";
//        String secretKey = "cYqOn7g5wDG0hp3OfgUytG0rvwtVKlsC";
//        PushKeyPair pair = new PushKeyPair(apiKey, secretKey);
//
//        // 2. build a BaidupushClient object to access released interfaces
//        BaiduPushClient pushClient = new BaiduPushClient(pair,
//                BaiduPushConstants.CHANNEL_REST_URL);
//
//        // 3. register a YunLogHandler to get detail interacting information
//        // in this request.
//        pushClient.setChannelLogHandler(new YunLogHandler() {
//            @Override
//            public void onHandle(YunLogEvent event) {
//                System.out.println(event.getMessage());
//            }
//        });
//
//        try {
//            // 4. specify request arguments
//            //创建 Android的通知
//            JSONObject notification = new JSONObject();
//            notification.put("title", "TEST");
//            notification.put("description","Hello Baidu Push");
//            notification.put("notification_builder_id", 0);
//            notification.put("notification_basic_style", 4);
//            notification.put("open_type", 1);
//            notification.put("url", "http://push.baidu.com");
//            JSONObject jsonCustormCont = new JSONObject();
//            jsonCustormCont.put("key", "测试"); //自定义内容，key-value
//            notification.put("custom_content", jsonCustormCont);
//
//            PushMsgToSingleDeviceRequest request = new PushMsgToSingleDeviceRequest()
//                    .addChannelId("3576521358822543553")
//                    .addMsgExpires(new Integer(3600)). // message有效时间
//                    addMessageType(0).// 1：通知,0:透传消息. 默认为0 注：IOS只有通知.
//                    addMessage(notification.toString()).
//                    addDeviceType(3);// deviceType => 3:android, 4:ios
//            // 5. http request
//            PushMsgToSingleDeviceResponse response = pushClient
//                    .pushMsgToSingleDevice(request);
//            // Http请求结果解析打印
//            System.out.println("msgId: " + response.getMsgId() + ",sendTime: "
//                    + response.getSendTime());
//        } catch (PushClientException e) {
//            /*
//             * ERROROPTTYPE 用于设置异常的处理方式 -- 抛出异常和捕获异常,'true' 表示抛出, 'false' 表示捕获。
//             */
//            if (BaiduPushConstants.ERROROPTTYPE) {
//                throw e;
//            } else {
//                e.printStackTrace();
//            }
//        } catch (PushServerException e) {
//            if (BaiduPushConstants.ERROROPTTYPE) {
//                throw e;
//            } else {
//                System.out.println(String.format(
//                        "requestId: %d, errorCode: %d, errorMessage: %s",
//                        e.getRequestId(), e.getErrorCode(), e.getErrorMsg()));
//            }
//        }
//    }
    }
}
