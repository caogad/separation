package com.scaffolding.separation.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.scaffolding.separation.common.ErrorCode;
import com.scaffolding.separation.config.EnvConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 生成Token工具类
 * auth:admin
 */
@Configuration
@EnableConfigurationProperties(EnvConfig.class)
public class JwtTokenUtils {

    /** token秘钥，请勿泄露，请勿随便修改 */
    public static final String SECRET = "separation_0001";
    /** token 过期时间: 10天 */
    public static final int calendarField = Calendar.DATE;
    public static final int calendarInterval = 10;

    /**
     * 用户登陆后生成Token
     * @param userId
     * @return
     */
    public String createToken(String userId){
        Date iatDate = new Date();
        // expire time
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(calendarField, calendarInterval);
        Date expiresDate = nowTime.getTime();

        // header Map
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        String token = JWT.create()
                .withHeader(map) // header
                .withClaim("iss", "Service") // payload iss: jwt签发者
                .withClaim("aud", "APP")// payload aud: 接收jwt的一方
                .withClaim("userId", null == userId ? null :userId)
                .withIssuedAt(iatDate) // sign time
                .withExpiresAt(expiresDate) // expire time
                .sign(Algorithm.HMAC256(SECRET)); // 以 SECRET 作为 token 的密钥

        return token;
    }

    /**
     * 解密Token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public  Map<String, Claim> verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            // token 校验失败, 抛出Token验证非法异常
            throw new RuntimeException(ErrorCode.HTTP_401.getValue());
        }
        return jwt.getClaims();
    }
    /**
     * 根据Token获取user_id
     *
     * @param token
     * @return user_id
     */
    public Long getUserId(String token) {
        Map<String, Claim> claims = verifyToken(token);
        Claim user_id_claim = claims.get("userId");
        if (null == user_id_claim || StringUtils.isEmpty(user_id_claim.asString())) {
            // token 校验失败, 抛出Token验证非法异常
            throw new RuntimeException(ErrorCode.DECRYPT_TOKEN.getValue());
        }
        return Long.valueOf(user_id_claim.asString());
    }
}
