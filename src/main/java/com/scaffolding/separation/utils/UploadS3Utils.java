package com.scaffolding.separation.utils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.scaffolding.separation.config.EnvConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Configuration
@EnableConfigurationProperties(EnvConfig.class)
public class UploadS3Utils {
    @Autowired
    private EnvConfig envConfig;
    // public AmazonS3 s3;
//    public  String AWS_ACCESS_KEY = "AKIART6FVJ6UP22NEANS"; // 【你的 access_key】
//    public  String AWS_SECRET_KEY = "aFEy5rit5wEad/KQQFN/V+PjQvIeQkLeb37RCjlM"; // 【你的 aws_secret_key】
//    public  String bucketName = "ped-research.igskapp.com"; // 【你 bucket 的名字】 # 首先需要保证 s3 上已经存在该存储桶

    public String uploadToS3(File tempFile) {
        //首先创建一个s3的客户端操作对象（需要amazon提供的密钥）
        AmazonS3 s3 = new AmazonS3Client(new BasicAWSCredentials(envConfig.getAWS_ACCESS_KEY(), envConfig.getAWS_SECRET_KEY()));
        Region usWest2 = Region.getRegion(Regions.CN_NORTH_1);
        s3.setRegion(usWest2);
        //设置bucket,key
        String fileName = tempFile.getName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        String key = UUID.randomUUID() + "." + suffix;
        try {
            String bucketPath = envConfig.getBucketName() + "/meterial";
            //验证名称为bucketName的bucket是否存在，不存在则创建
            if (!checkBucketExists(s3, envConfig.getBucketName())) {
                s3.createBucket(envConfig.getBucketName());
            }
            s3.putObject(new PutObjectRequest(bucketPath, key, tempFile)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(bucketPath, key);
            URL url = s3.generatePresignedUrl(urlRequest);
            String s = url.toString();
            int location = s.indexOf("?");
            String str = s.substring(0, location);
            return str;
        } catch (AmazonServiceException ase) {
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        }
        return null;
    }

    /**
     * 验证s3上是否存在名称为bucketName的Bucket
     *
     * @param s3
     * @param bucketName
     * @return
     */
    public boolean checkBucketExists(AmazonS3 s3, String bucketName) {
        List<Bucket> buckets = s3.listBuckets();
        for (Bucket bucket : buckets) {
            if (Objects.equals(bucket.getName(), bucketName)) {
                return true;
            }
        }
        return false;
    }

}
