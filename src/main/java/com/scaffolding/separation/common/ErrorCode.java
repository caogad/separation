package com.scaffolding.separation.common;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ErrorCode implements IEnum<String> {

    TEST(1000, "测试错误编码"),
    DECRYPT_TOKEN(1001, "Token验证非法."),
    USER_NAMW_NULL_ERROR(10000,"用户名不能为空."),
    USER_NAMW_ERROR(10001,"用户名不存在."),
    PASSWORD_NULL_EROOR(10002,"密码不能为空."),
    PASSWORD_EROOR(10003,"密码不正确."),
    HTTP_401(10004,"HTTP 401 错误 - 未授权."),
    ICON_URL(10005,"头像不能为空");

    private long code;
    private String message;

    ErrorCode(final long code, final String message) {
        this.code = code;
        this.message = message;
    }


    public long getCode() {
        return this.code;
    }

    @Override
    public String getValue() {
        return this.message;
    }
}
